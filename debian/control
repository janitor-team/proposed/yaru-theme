Source: yaru-theme
Section: misc
Priority: optional
Maintainer: Debian Desktop Theme Team <yaru-theme@packages.debian.org>
Uploaders: Martin Wimpress <code@flexion.org>,
           Mike Gabriel <sunweaver@debian.org>,
           Debian+Ubuntu MATE Packaging Team <debian-mate@lists.debian.org>
Build-Depends: debhelper-compat (= 13),
#               dh-migrations,
Build-Depends-Indep: libglib2.0-dev,
                     meson (>= 0.59),
                     sassc,
Standards-Version: 4.6.1
Rules-Requires-Root: no
Homepage: https://github.com/ubuntu/yaru
Vcs-Browser: https://salsa.debian.org/desktop-themes-team/yaru-theme
Vcs-Git: https://salsa.debian.org/desktop-themes-team/yaru-theme.git

Package: yaru-theme-gnome-shell
Architecture: all
Depends: ${shlibs:Depends},
         ${misc:Depends},
Breaks: gnome-shell (<< 43~)
Description: Yaru GNOME Shell desktop theme from the Ubuntu Community
 This is the theme, better than a burrito, that is shaped by the community
 on the Ubuntu discourse.
 .
 This package contains the GNOME Shell theme part
 .
 If you want to follow development, more information here:
 https://community.ubuntu.com/c/desktop/theme-refresh.

Package: yaru-theme-gtk
Architecture: all
Depends: ${shlibs:Depends},
         ${misc:Depends},
         gnome-themes-extra,
         gtk2-engines-pixbuf,
         gtk2-engines-murrine,
         libglib2.0-bin,
Description: Yaru GTK theme from the Ubuntu Community
 This is the theme, better than a burrito, that is shaped by the community
 on the Ubuntu discourse.
 .
 This package contains the GTK+ 2 and 3 theme parts
 .
 If you want to follow development, more information here:
 https://community.ubuntu.com/c/desktop/theme-refresh.

Package: yaru-theme-icon
Architecture: all
Depends: ${shlibs:Depends},
         ${misc:Depends},
Description: Yaru icon theme from the Ubuntu Community
 This is the theme, better than a burrito, that is shaped by the community
 on the Ubuntu discourse.
 .
 This package contains the icon theme
 .
 If you want to follow development, more information here:
 https://community.ubuntu.com/c/desktop/theme-refresh.

Package: yaru-theme-sound
Architecture: all
Depends: ${shlibs:Depends},
         ${misc:Depends},
Description: Yaru sound theme from the Ubuntu Community
 This is the theme, better than a burrito, that is shaped by the community
 on the Ubuntu discourse.
 .
 This package contains the sound theme
 .
 If you want to follow development, more information here:
 https://community.ubuntu.com/c/desktop/theme-refresh.

Package: yaru-theme-unity
Architecture: all
Depends: ${shlibs:Depends},
         ${misc:Depends},
Description: Yaru Unity theme from the Ubuntu Community
 This is the theme, better than a burrito, that is shaped by the community
 on the Ubuntu discourse.
 .
 This package contains the Unity theme parts
 .
 If you want to follow development, more information here:
 https://community.ubuntu.com/c/desktop/theme-refresh.
